package com.openweathermap.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.openweathermap.model.CityWeather;

@Service
public class WeatherService implements IWeatherService {

	@Autowired
	private Environment environment;

	List<CityWeather> lstLocationWeather;

	@Override
	public List<CityWeather> getCitiesWeather() throws JSONException {

		String uri = buildOpenWeatherMapURI();

		RestTemplate restTemplate = new RestTemplate();
		String strResult = restTemplate.getForObject(uri.toString(), String.class);
		JSONObject result = new JSONObject(strResult);

		JSONArray citiesWeather = result.getJSONArray("list");
		lstLocationWeather = new ArrayList<CityWeather>();
		for (int i = 0; i < citiesWeather.length(); i++) {
			JSONObject cityWeather = citiesWeather.getJSONObject(i);
			if (cityWeather != null) {
				CityWeather locWeather = new CityWeather();
				locWeather.setLocation(cityWeather.getString("name"));
				locWeather.setTemperature(Double.toString(cityWeather.getJSONObject("main").getDouble("temp")) + " °C");

				JSONArray weather = cityWeather.getJSONArray("weather");
				StringBuilder sbWeather = new StringBuilder();
				for (int j = 0; j < weather.length(); j++) {

					sbWeather.append(weather.getJSONObject(j).getString("main"));
					if (j != weather.length() - 1) {
						sbWeather.append(", ");
					}
				}

				locWeather.setActualWeather(sbWeather.toString());
				lstLocationWeather.add(locWeather);
			}
		}
		return lstLocationWeather;
	}

	@Override
	public void saveresponse(ResponseEntity<List<CityWeather>> response) {

		String uri = environment.getProperty("home-credit-api2.uri");
		RestTemplate restTemplate = new RestTemplate();

		restTemplate.postForObject(uri, response, String.class);
	}

	private String buildOpenWeatherMapURI() {
		StringBuilder uri = new StringBuilder();
		uri.append(environment.getProperty("openweathermap.uri"));
		uri.append("group?id=");
		uri.append(environment.getProperty("openweathermap.citiesid"));
		uri.append("&units=metric&APPID=");
		uri.append(environment.getProperty("openweathermap.appid"));

		return uri.toString();
	}

}
