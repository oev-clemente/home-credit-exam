package com.openweathermap.service;

import java.util.List;

import org.json.JSONException;
import org.springframework.http.ResponseEntity;

import com.openweathermap.model.CityWeather;

public interface IWeatherService {

	public List<CityWeather> getCitiesWeather() throws JSONException;

	public void saveresponse(ResponseEntity<List<CityWeather>> response);
}
