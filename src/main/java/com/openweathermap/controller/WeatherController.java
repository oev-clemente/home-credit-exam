package com.openweathermap.controller;

import java.util.List;
import java.util.UUID;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.openweathermap.model.CityWeather;
import com.openweathermap.service.IWeatherService;;

@RestController
public class WeatherController {

	List<CityWeather> cityWeatherList;

	@Autowired
	IWeatherService weatherService;

	@GetMapping(value = "/weather/getallweather", produces = "application/json")
	public ResponseEntity<List<CityWeather>> getAllWeather() throws JSONException {

		cityWeatherList = weatherService.getCitiesWeather();
		String uniqueID = UUID.randomUUID().toString();
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("GUID", uniqueID);

		ResponseEntity<List<CityWeather>> response = new ResponseEntity<>(cityWeatherList, responseHeaders, HttpStatus.OK);

		weatherService.saveresponse(response);
		return response;
	}
}
