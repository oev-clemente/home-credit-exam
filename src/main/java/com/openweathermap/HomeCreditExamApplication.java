package com.openweathermap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeCreditExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeCreditExamApplication.class, args);
	}
}
